using Avalonia.Controls;
using System;
using System.Collections.Generic;
using Avalonia;
using Avalonia.Input;
using Avalonia.Markup.Xaml;
using Avalonia.Threading;
using NAudio.Wave;

namespace Tarefa4._5_Code.Views;

public partial class MainWindow : Window
{
    private readonly Image? _imageCar;
    private readonly Image? _imageCarM;
    public static int count = 0;
    public static double newXship;
    public static double newYship;
    private double _delayBetweenShots = 0;
    private DispatcherTimer? timer;
    private WaveOutEvent waveOut;
    private AudioFileReader? audioFileReader;


    private DispatcherTimer _timer;
    private readonly HashSet<Key> _pressedKeys = new HashSet<Key>();

    public MainWindow()
    {
        InitializeComponent();
        this.AttachDevTools();
        _imageCar = this.FindControl<Image>("Car");
        _imageCarM = this.FindControl<Image>("CarM");
        KeyDown += HandleKeyDown;
        KeyUp += HandleKeyUp;

       
        var gameTimer = new DispatcherTimer
        {
            Interval = TimeSpan.FromMilliseconds(16.67) 
        };
        gameTimer.Tick += GameLoop;
        gameTimer.Start();
    }

    private void InitializeComponent()
    {
        AvaloniaXamlLoader.Load(this);
    }

    private void HandleKeyDown(object sender, KeyEventArgs e)
    {
        if (!_pressedKeys.Contains(e.Key))
        {
            _pressedKeys.Add(e.Key);
        }
    }

    private void HandleKeyUp(object sender, KeyEventArgs e)
    {
        if (_pressedKeys.Contains(e.Key))
        {
            _pressedKeys.Remove(e.Key);
        }
    }

    private void GameLoop(object sender, EventArgs e)
    {
        double moveAmount = 10.30;

        if (count == 0)
        {
            count++;
            MoveHome();
        }

        foreach (Key key in _pressedKeys)
        {
            switch (key)
            {
                case Key.Space:
                    PlayMusic();
                    break;
                case Key.Left:
                    MoveLeft(moveAmount);
                    break;
                case Key.Right:
                    MoveRight(moveAmount);
                    break;
                case Key.Up:
                    MoveUp(moveAmount);
                    break;
                case Key.Down:
                    MoveDown(moveAmount);
                    break;
            }
        }
    }

    private void MoveLeft(double moveAmount)
    {
        if (_imageCar != null)
        {
            newXship = Canvas.GetLeft(_imageCar) - moveAmount;
            newXship = Canvas.GetLeft(_imageCarM) - moveAmount;

            if (newXship >= 0)
            {
                Canvas.SetLeft(_imageCar, newXship);
                Canvas.SetLeft(_imageCarM, newXship);

            }
            else
            {
                Canvas.SetLeft(_imageCar, 0);
                Canvas.SetLeft(_imageCar, 0);
            }
        }
    }

    private void MoveRight(double moveAmount)
    {
        if (_imageCar != null)
        {
            newXship = Canvas.GetLeft(_imageCar) + moveAmount;
            newXship = Canvas.GetLeft(_imageCarM) + moveAmount;

            if (newXship < Width - 70)
            {
                Canvas.SetLeft(_imageCar, newXship);
                Canvas.SetLeft(_imageCarM, newXship);
            }
            else
            {
                Canvas.SetLeft(_imageCar, Width -70);
                Canvas.SetLeft(_imageCarM, Width -70);

            }
        }
    }
    
    private void MoveDown(double moveAmount)
    {
        if (_imageCar != null)
        {
            newYship = Canvas.GetTop(_imageCar) + moveAmount;
            newYship = Canvas.GetTop(_imageCarM) + moveAmount;

            if (newYship < Height -140)
            {
                Canvas.SetTop(_imageCar, newYship);
                Canvas.SetTop(_imageCarM, newYship);

            }
            else
            {
                Canvas.SetTop(_imageCar, Height -140);
                Canvas.SetTop(_imageCarM, Height -140);
            }
            
        }
    }
    
   
    private void MoveUp(double moveAmount)
    {
        if (_imageCar != null)
        {
            newYship = Canvas.GetTop(_imageCar) - moveAmount;
            newYship = Canvas.GetTop(_imageCarM) - moveAmount;

            if (newYship > 0)
            {
                Canvas.SetTop(_imageCar, newYship);
                Canvas.SetTop(_imageCarM, newYship);

            }
            else
            {
                Canvas.SetTop(_imageCar, 0);
                Canvas.SetTop(_imageCarM, 0);

            }
           
        }
    }

    private void MoveHome()
    {
        if (_imageCar != null)
        {
          
            Canvas.SetLeft(_imageCar, 0);
            Canvas.SetTop(_imageCar, 0);
            Canvas.SetLeft(_imageCarM, 0);
            Canvas.SetTop(_imageCarM, 0);

        }
    }
    
    private void PlayMusic()
    {
       

        if (_imageCar.IsVisible == false)
        {
            _imageCar.IsVisible = true;
            _imageCarM.IsVisible = false;
            
        }
        else
        {
            _imageCar.IsVisible = false;
            _imageCarM.IsVisible = true;
        }
        
        string audioFilePath = "../../../Assets/A.mp3";

        if (System.IO.File.Exists(audioFilePath))
        {
            
            StopMusic();
            waveOut = new WaveOutEvent();
            audioFileReader = new AudioFileReader(audioFilePath);
            waveOut.PlaybackStopped += (sender, args) =>
            {
                waveOut.Dispose();
                audioFileReader.Dispose();
            };
            
            waveOut.Init(audioFileReader);
            
            waveOut.Play();
        }
    }

    private void StopMusic()
    {
        if (waveOut != null)
        {
            waveOut.Stop();
            waveOut.Dispose();
        }
        if (audioFileReader != null)
        {
            audioFileReader.Dispose();
        }
    }

}